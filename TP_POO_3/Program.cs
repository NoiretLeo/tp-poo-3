﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Dinosaur denver = new Dinosaur("Denver", "Last", 57);
            Dinosaur dorito = new Dinosaur("Dorito", "Doritosaurus", 44);

            Horde horde = new Horde();
            horde.add(louis);
            horde.add(nessie);
            horde.add(denver);
            horde.add(dorito);

            horde.greetings();

            Console.ReadLine();
        }
    }
}
