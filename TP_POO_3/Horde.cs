﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicConsole
{
    public class Horde
    {
        private List<Dinosaur> dinosaurs;

        public Horde()
        {
            this.dinosaurs = new List<Dinosaur>();
        }

        public List<Dinosaur> getDinosaurs()
        {
            return this.dinosaurs;
        }

        public void add(Dinosaur dino)
        {
            this.dinosaurs.Add(dino);
        }

        public void remove(Dinosaur dino)
        {
            this.dinosaurs.Remove(dino);
        }

        public string greetings()
        {
            string greets = "";

            if(this.dinosaurs.Count != 0)
            {
                foreach (Dinosaur dino in this.dinosaurs)
                {
                    greets = String.Format("{0}{1}\n", greets, dino.sayHello());
                }
            }

            return greets;
        }
    }
}
