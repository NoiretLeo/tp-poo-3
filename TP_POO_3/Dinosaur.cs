﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicConsole
{
    public class Dinosaur
    {
        private string name;
        private string specie;
        private int age;

        public Dinosaur(string name, string specie, int age)
        {
            this.name = name;
            this.specie = specie;
            this.age = age;
        }

        public string sayHello()
        {
            return String.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, this.specie, this.age);
        }

        public string roar()
        {
            return "Grrr";
        }

        public string hug(Dinosaur dino)
        {
            return String.Format("Je suis {0} et je fais un calin à {1}", this.name, dino.getName());
        }

        public string getName()
        {
            return this.name;
        }

        public string getSpecie()
        {
            return this.specie;
        }

        public int getAge()
        {
            return this.age;
        }

        public void setName(string name)
        {
            this.name = name;
        }

        public void setSpecie(string specie)
        {
            this.specie = specie;
        }

        public void setAge(int age)
        {
            this.age = age;
        }
    }
}
