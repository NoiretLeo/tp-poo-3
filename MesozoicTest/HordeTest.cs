﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MesozoicConsole;

namespace MesozoicTest
{
    [TestClass]
    public class HordeTest
    {
        [TestMethod]
        public void TestAdd()
        {
            Horde horde = new Horde();

            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Dinosaur denver = new Dinosaur("Denver", "Last", 57);
            Dinosaur dorito = new Dinosaur("Dorito", "Doritosaurus", 44);

            Assert.AreEqual(0, horde.getDinosaurs().Count);
            horde.add(louis);
            Assert.AreEqual(1, horde.getDinosaurs().Count);
            horde.add(nessie);
            Assert.AreEqual(2, horde.getDinosaurs().Count);
            horde.add(denver);
            Assert.AreEqual(3, horde.getDinosaurs().Count);
            horde.add(dorito);
            Assert.AreEqual(4, horde.getDinosaurs().Count);
        }

         [TestMethod]
         public void TestRemove()
        {
            Horde horde = new Horde();

            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Dinosaur denver = new Dinosaur("Denver", "Last", 57);
            Dinosaur dorito = new Dinosaur("Dorito", "Doritosaurus", 44);

            horde.add(louis);
            horde.add(nessie);
            horde.add(denver);
            horde.add(dorito);

            Assert.AreEqual(4, horde.getDinosaurs().Count);
            horde.remove(denver);
            Assert.AreEqual(3, horde.getDinosaurs().Count);
            horde.remove(dorito);
            Assert.AreEqual(2, horde.getDinosaurs().Count);
            horde.remove(louis);
            Assert.AreEqual(1, horde.getDinosaurs().Count);
            horde.remove(nessie);
            Assert.AreEqual(0, horde.getDinosaurs().Count);
        }

        [TestMethod]
        public void TestGreetings()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);

            Horde horde = new Horde();

            Assert.AreEqual("", horde.greetings());

            horde.add(louis);
            Assert.AreEqual("Je suis Louis le Stegosaurus, j'ai 12 ans.\n", horde.greetings());
            horde.add(nessie);
            Assert.AreEqual("Je suis Louis le Stegosaurus, j'ai 12 ans.\nJe suis Nessie le Diplodocus, j'ai 11 ans.\n", horde.greetings());
        }
    }
}
